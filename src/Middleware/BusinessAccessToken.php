<?php

namespace Ineplant\Middleware;

use Closure;
use Ineplant\Helper;

class BusinessAccessToken {

    const AUTH_KEY = 'HTTP_AUTHORIZATION';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $tokenBody = Helper::resolveToken($_SERVER[BusinessAccessToken::AUTH_KEY]);

        return $next($request);
    }
}
<?php

namespace Ineplant\Middleware;

use Closure;
use Ineplant\Helper;

class ConsumeAccessToken {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $tokenBody = Helper::resolveToken($_SERVER[BusinessAccessToken::AUTH_KEY]);

        return $next($request);
    }
}
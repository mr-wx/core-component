<?php

namespace Ineplant;

use Ineplant\Enum\ErrorCode;
use Ineplant\BaseClass\Response;

final class ApiResponse {

    /**
     * ApiResponse constructor. 禁止初始化
     */
    private function __construct()
    {

    }


    /**
     *
     *
     {
        "Code": 2,
        "Message": "账号密码错误",
        "ErrorCode": 1,
        "OtherCode": null,
        "ErrorMessages": {},
        "Content": null
    }Code为0就是成功，2是自定义异常，3是服务异常
     *
     * 处理api返回数据格式
     *
     * @param mixed $response
     * @param int $errorCode
     * @return array
     */
    public static function handler($response = '', $errorCode = 0){
        $resp= new Response();

        if (0 != $errorCode) {
            $resp->setCode(2);
            $resp->setErrorCode($errorCode);
            $resp->setMessage($response ?: ErrorCode::toString($errorCode));
        } else {
            $resp->setContent($response);
        }

        return $resp->toArray();
    }

    /**
     * 输出json格式，通过状态码自动匹配错误信息
     *
     * @param $errCode
     * @param string $msg
     * @return array
     */
    public static function out($errCode, $msg = '') {
        return self::handler($msg, $errCode);
    }

    /**
     * 列表查询没有数据时的返回
     *
     * @return array
     */
    public static function noData()
    {
        return self::handler([
            'data' => [],
            'total' => 0
        ]);
    }

    /**
     * 禁止克隆
     */
    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

}
<?php
namespace Ineplant\BaseClass;

class Response {

    /**
     * @var int
     */
    protected $code = 0;

    /**
     * @var string
     */
    protected $message = '';

    /**
     * @var int
     */
    protected $errorCode = 0;

    /**
     * @var string
     */
    protected $otherCode = '';

    /**
     * @var string|null
     */
    protected $errorMessages = null;

    /**
     * @var string
     */
    protected $content = '';

    /**
     * Response constructor.
     *
     * @param $code
     * @param string $message
     * @param string $content
     * @param int $errorCode
     * @param string $errorMessages
     * @param string $otherCode
     */
    public function __construct($code, $message = '', $content = '', $errorCode = 0, $errorMessages = '', $otherCode = '') {
        $this->code          = $code;
        $this->message       = $message;
        $this->content       = $content;
        $this->errorCode     = $errorCode;
        $this->errorMessages = $errorMessages;
        $this->otherCode     = $otherCode;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'Code'          => $this->code,
            'Message'       => $this->message,
            'ErrorCode'     => $this->errorCode,
            'OtherCode'     => $this->otherCode,
            'ErrorMessages' => $this->errorMessages, // 一般用于验证，存放具体的验证错误信息
            'Content'       => $this->content,
        ];
    }

    /**
     * @param int $code
     */
    public function setCode(int $code) {
        $this->code = $code;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message) {
        $this->message = $message;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode(int $errorCode) {
        $this->errorCode = $errorCode;
    }

    /**
     * @param string $otherCode
     */
    public function setOtherCode(string $otherCode) {
        $this->otherCode = $otherCode;
    }

    /**
     * @param $errorMessages
     */
    public function setErrorMessages($errorMessages) {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @param $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

}
<?php

namespace Ineplant\BaseClass;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

trait RequestTrait {

    /**
     * 重写验证失败时的异常抛出
     *
     * @param Validator $validator
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator) {

        throw new ValidationException($validator, response(
            $this->formatErrors($validator)));

    }


    /**
     * 格式化输出错误消息
     *
     * @param Validator $validator
     * @return array
     */
    protected function formatErrors(Validator $validator) {
        $resp = new Response(1, '参数错误', '', 400, $validator->errors()->all());

        return $resp->toArray();
    }

}